### Compile

1. HOST:

```shell
$ ./build-cv3.sh <path to tengine_khadas_sdk>
```

2. LOCAL:

```shell
$ ./build-cv3.sh
```

### Run

```shell
$ ./tengine_khadas_yolov3_camera -m <path to yolov3 uint8_t timfile> -i < path to picture>
```


